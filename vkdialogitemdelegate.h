#ifndef VKDIALOGITEMDELEGATE_H
#define VKDIALOGITEMDELEGATE_H

#include <QItemDelegate>
#include <QDebug> //TODO REMOVE
#include <QPainter>
#include <QEvent>
#include <QTextDocument>

class MainWindow;//Predefenition)))


#include <vkdialogentity.h>

class VkDialogItemDelegate: public QItemDelegate
{
    Q_OBJECT
public:
    VkDialogItemDelegate(MainWindow *parent = nullptr);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const override;
    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const override;
    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
private:
    MainWindow * mainWindow;
};

#endif // VKDIALOGITEMDELEGATE_H
