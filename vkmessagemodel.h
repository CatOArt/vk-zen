#ifndef VKMESSAGEMODEL_H
#define VKMESSAGEMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QDebug> //TODO REMOV
#include <QStringList>
#include <QList>

#include <vkmessageentity.h>

class VkMessageModel: public QAbstractListModel
{
    Q_OBJECT
public:
    VkMessageModel();
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void append(VkMessageEntity*);
public://TODO private
    QList<VkMessageEntity*> entities;
};

#endif // VKMESSAGEMODEL_H
