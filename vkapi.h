#ifndef VKAPI_H
#define VKAPI_H

#include <QObject>
#include <memory>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


#include "vkdialogsmodel.h"
#include "vkmessagemodel.h"

class Vkapi: public QObject
{

    Q_OBJECT

public:
    Vkapi();
    void setDialogsModel(VkDialogsModel*);
    void setMessageModel(VkMessageModel *);
    QUrl getAuthUrl();
    void setToken(QString);
    void getDialogs();
    void getMessages(int id);
public: //TODO make private
    QString token = "";
    VkDialogsModel* dialogsModel;
    VkMessageModel* messageModel;
    QString appCode = "6701018";
    QString authUrl = "https://oauth.vk.com/authorize?client_id=%1&display=mobile&redirect_uri="
                      "https://oauth.vk.com/blank.html&scope=4098&response_type=token&v=5.37";
    std::unique_ptr<QNetworkAccessManager> networkManager;


private slots:
    void proceedDialogs(QNetworkReply *reply);
    void proceedMessages(QNetworkReply *reply);
    void getReply(QNetworkReply* reply);
};

#endif // VKAPI_H
