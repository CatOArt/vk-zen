#ifndef VKDIALOGSMODEL_H
#define VKDIALOGSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QDebug> //TODO REMOV
#include <QStringList>
#include <QList>

#include "vkdialogentity.h"

class VkDialogsModel: public QAbstractListModel
{
    Q_OBJECT
public:
    VkDialogsModel();
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    void append(VkDialogEntity*);
public://TODO private
    QList<VkDialogEntity*> entities;
};

#endif // VKDIALOGSMODEL_H
