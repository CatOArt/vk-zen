#ifndef VKMESSAGEITEMDELEGATE_H
#define VKMESSAGEITEMDELEGATE_H

#include <QItemDelegate>
#include <QDebug> //TODO REMOVE
#include <QPainter>
#include <QEvent>
#include <QTextDocument>

class MainWindow;//Predefenition)

class VkMessageItemDelegate: public QItemDelegate
{
    Q_OBJECT
public:
    VkMessageItemDelegate(MainWindow *parent = nullptr);
    void paint(QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index) const override;
    QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const override;
    bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;
private:
    MainWindow * mainWindow;
};

#endif // VKMESSAGEITEMDELEGATE_H
