#include "vkmessagemodel.h"

VkMessageModel::VkMessageModel()
{

}

int VkMessageModel::rowCount(const QModelIndex &parent) const
{
    return entities.length();
}

QVariant VkMessageModel::data(const QModelIndex &index, int role) const
{
    return QVariant::fromValue(entities[index.row()]);
}

void VkMessageModel::append(VkMessageEntity *entity)
{
    beginInsertRows(QModelIndex(), 0, rowCount(QModelIndex()));//не уверен что я правильно это написал
    entities.prepend(entity);
    endInsertRows();
}
