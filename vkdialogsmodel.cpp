#include "vkdialogsmodel.h"

VkDialogsModel::VkDialogsModel()
{

}

int VkDialogsModel::rowCount(const QModelIndex &parent) const
{
    return entities.length();
}

QVariant VkDialogsModel::data(const QModelIndex &index, int role) const
{
    return QVariant::fromValue(entities[index.row()]);
}

void VkDialogsModel::append(VkDialogEntity *entity)
{
    beginInsertRows(QModelIndex(), 0, rowCount(QModelIndex()));//не уверен что я правильно это написал
    entities.append(entity);
    endInsertRows();
}

