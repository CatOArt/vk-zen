#include "mainwindow.h"
#include <QApplication>

#include "vkdialogentity.h"
#include "vkmessageentity.h"
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    qRegisterMetaType<VkDialogEntity>();
    qRegisterMetaType<VkMessageEntity>();
    w.show();

    return a.exec();
}
