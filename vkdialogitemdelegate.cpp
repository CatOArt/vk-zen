#include "vkdialogitemdelegate.h"
#include "mainwindow.h"


VkDialogItemDelegate::VkDialogItemDelegate(MainWindow *parent):
    QItemDelegate (parent)
{
    mainWindow = parent;
}

void VkDialogItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();
    VkDialogEntity* dialogEntity =  qvariant_cast<VkDialogEntity*>(index.data());
    QTextDocument td;
    td.setHtml(dialogEntity->name+"<br>"+dialogEntity->lastMessage+ "<br>" +QString::number(dialogEntity->id) + " " + dialogEntity->type);
    painter->fillRect(option.rect, option.palette.highlight());
    QRect grect(25,25,50,50);    grect.moveCenter(option.rect.center());
    painter->fillRect(grect, option.palette.midlight());
    painter->translate(option.rect.topLeft());
    td.drawContents(painter);
    painter->restore();
}

QSize VkDialogItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(300,100);
}

bool VkDialogItemDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{

    if (event->type() == QEvent::MouseButtonRelease){
        VkDialogEntity* dialogEntity =  qvariant_cast<VkDialogEntity*>(index.data());
        mainWindow->mVkapi->getMessages(dialogEntity->id);
    }
    return true; //idk what to return;
}
