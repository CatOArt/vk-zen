#-------------------------------------------------
#
# Project created by QtCreator 2018-09-22T17:18:25
#
#-------------------------------------------------

QT       += core gui
QT       += network
QT       += webenginewidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vk-wind
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lssl -lcrypto

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    vkapi.cpp \
    vkdialogsmodel.cpp \
    vkdialogitemdelegate.cpp \
    vkdialogentity.cpp \
    vkmessageentity.cpp \
    vkmessageitemdelegate.cpp \
    vkmessagemodel.cpp \
    vkuserentity.cpp

HEADERS += \
        mainwindow.h \
    vkapi.h \
    vkdialogsmodel.h \
    vkdialogitemdelegate.h \
    vkdialogentity.h \
    vkmessageentity.h \
    vkmessageitemdelegate.h \
    vkmessagemodel.h \
    vkuserentity.h

FORMS += \
        mainwindow.ui
