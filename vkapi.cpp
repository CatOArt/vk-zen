#include "vkapi.h"


Vkapi::Vkapi()
{
    networkManager = std::make_unique<QNetworkAccessManager>(this);
    connect(networkManager.get(), &QNetworkAccessManager::finished,this,&Vkapi::getReply);
}

void Vkapi::setDialogsModel(VkDialogsModel * model)
{
    dialogsModel = model;
}

QUrl Vkapi::getAuthUrl()
{
    return QUrl(authUrl.arg(appCode));
}

void Vkapi::setToken(QString _token)
{
    token = _token;
}

void Vkapi::getDialogs()
{
    static const QString url_template = "https://api.vk.com/method/messages.getConversations?"
                                        "access_token=%1"
                                        "&offset=0"
                                        "&count=10"
                                        "&filter=all"
                                        "&v=5.85 "
                                        "&extended=1";
    QUrl url = QUrl(url_template.arg(token));
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::User,"getDialogs");
    networkManager->get(request);
}

void Vkapi::getMessages(int id)
{
    static const QString url_template = "https://api.vk.com/method/messages.getHistory?"
                                        "access_token=%1"
                                        "&offset=0"
                                        "&count=20"
                                        "&peer_id=%2"
                                        "&v=5.85"
                                        "&extended=1";
    QUrl url = QUrl(url_template.arg(token,QString::number(id)));
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::User,"getMessages");
    networkManager->get(request);
}

void Vkapi::setMessageModel(VkMessageModel *value)
{
    messageModel = value;
}

void Vkapi::proceedDialogs(QNetworkReply *reply)
{
    QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
    QJsonObject response = document.object()["response"].toObject();
    QJsonArray dialogs = response["items"].toArray();
    QJsonArray profiles = response["profiles"].toArray();
    for(QJsonValueRef dialogRef :dialogs){
        VkDialogEntity* entity = new VkDialogEntity();
        QJsonObject dialog = dialogRef.toObject()["conversation"].toObject();
        QJsonObject peer = dialog["peer"].toObject();
        QString type = peer["type"].toString();
        QString name;
        int id = peer["id"].toInt();
        if(type == "user"){
            entity->type = "user";
            for(QJsonValueRef profile: profiles){
                if (profile.toObject()["id"].toInt() == id){
                    name = profile.toObject()["first_name"].toString() +" "+ profile.toObject()["last_name"].toString();
                    break;
                }
            }
        }
        if(type == "chat"){
            entity->type = "chat";
            name = dialog["chat_settings"].toObject()["title"].toString();
        }
        QJsonObject last_message = dialogRef.toObject()["last_message"].toObject();
        QString text = last_message["text"].toString();
        entity->id = id;
        entity->name = name;
        entity->lastMessage = text;
        dialogsModel->append(entity);
    }
}

void Vkapi::proceedMessages(QNetworkReply *reply)
{
    QJsonDocument document = QJsonDocument::fromJson(reply->readAll());
    QJsonObject response = document.object()["response"].toObject();
    QJsonArray messages = response["items"].toArray();
    QJsonArray profiles = response["profiles"].toArray();
    for(QJsonValueRef messageRef: messages){
        VkMessageEntity* entity = new VkMessageEntity();
        QJsonObject message = messageRef.toObject();
        QString text = message["text"].toString();
        int id = message["from_id"].toInt();
        QString name;
        for(QJsonValueRef profile: profiles){
            if (profile.toObject()["id"].toInt() == id){
                name = profile.toObject()["first_name"].toString() +" "+ profile.toObject()["last_name"].toString();
                break;
            }
        }
        entity->id = id;
        entity->name = name;
        entity->message = text;
        messageModel->append(entity);
    }
}

void Vkapi::getReply(QNetworkReply *reply)
{
    QString type = reply->request().attribute(QNetworkRequest::User).toString();
    if (type == "getDialogs"){
        proceedDialogs(reply);
    }
    if (type == "getMessages"){
        proceedMessages(reply);
    }
}


