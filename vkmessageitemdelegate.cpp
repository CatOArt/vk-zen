#include "vkmessageitemdelegate.h"
#include "mainwindow.h"

VkMessageItemDelegate::VkMessageItemDelegate(MainWindow *parent):
    QItemDelegate (parent)
{
    mainWindow = parent;
}


void VkMessageItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    painter->save();
    VkMessageEntity* messageEntity =  qvariant_cast<VkMessageEntity*>(index.data());
    QTextDocument td;
    td.setHtml(messageEntity->name+"<br>"+messageEntity->message);
    painter->fillRect(option.rect, option.palette.highlight());
    QRect grect(25,25,50,50);    grect.moveCenter(option.rect.center());
    painter->fillRect(grect, option.palette.midlight());
    painter->translate(option.rect.topLeft());
    td.drawContents(painter);
    painter->restore();
}

QSize VkMessageItemDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(300,50);
}

bool VkMessageItemDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    //TODO: mb should do smthng
    return true; //idk what to return;
}
