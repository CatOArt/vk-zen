#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug> //TODO:remove debug

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    mVkapi = std::make_unique<Vkapi>();
    setWindowFlag(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    // Dialog view set
    VkDialogItemDelegate * dialogDelegate = new VkDialogItemDelegate(this);
    VkDialogsModel * dialogModel = new VkDialogsModel();
    ui->dialogList->setModel(dialogModel);
    ui->dialogList->setItemDelegate(dialogDelegate);
    mVkapi->setDialogsModel(dialogModel);

    VkMessageItemDelegate * messageDelegate = new VkMessageItemDelegate(this);
    VkMessageModel * messageModel = new VkMessageModel();
    ui->messageList->setModel(messageModel);
    ui->messageList->setItemDelegate(messageDelegate);
    mVkapi->setMessageModel(messageModel);


    //settings.remove("token"); //TODO: check if token expire;
    ui->browser->setVisible(false);
    if(settings.contains("token")){
        mVkapi->setToken(settings.value("token").toString());
    } else{
        ui->browser->load(mVkapi->getAuthUrl());
        ui->browser->setVisible(true);
        qDebug() << mVkapi->getAuthUrl();
        connect(ui->browser, &QWebEngineView::urlChanged,[=](QUrl url){

            QStringList parts = url.toString().split("access_token=");
            if(parts.length() > 1){
                ui->browser->setVisible(false);
                QString token = parts[1].split("&")[0];
                mVkapi->setToken(token);
                settings.setValue("token",token);
            }

        });
    };
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    mVkapi->getDialogs();
}
