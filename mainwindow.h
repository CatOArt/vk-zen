#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QUrl>
#include "vkapi.h"
#include <QByteArray>
#include <QWebEngineView>
#include <QUrl>
#include <QListView>
#include <memory>
#include <vkdialogitemdelegate.h>
#include <vkdialogsmodel.h>
#include <vkmessageitemdelegate.h>
#include <vkmessagemodel.h>
#include <QWebEnginePage>
#include <QWebEngineProfile>
#include <QWebEngineCookieStore>
#include <QSettings>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QSettings settings;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    std::unique_ptr<Vkapi> mVkapi;


private slots:
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
